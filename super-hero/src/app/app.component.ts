import { Component, OnInit } from '@angular/core';
import { Heroes } from './models/heroes.model';
import { HeroesService } from './service/heroes.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  HeroArray: Array<Heroes> = [];
  RecordCount!: string;

  constructor(private HeroesService: HeroesService, private router: Router) { }  

  ngOnInit(): void {

    this.HeroesService.getHero().subscribe((data: any) => {
      this.HeroArray = data; 
      this.RecordCount = this.HeroArray.length.toString(); 
    })    
  }

  DataByName(name: string) {    

      this.HeroesService.getDataByName(name).subscribe((data: any) => {
        this.HeroArray = (data);
      });
  }

  getHeroData(selectedValue: string): void {    
    this.router.navigate(['/change-hero'],
      {
        queryParams: {
          HeroName: selectedValue
        }
      }
    );
  }

  showPopup = false;
  showPopupWin(): void {
    this.showPopup = true;
  }
  
  closePopup(): void {
    this.showPopup = false;
    window.location.reload();
  }
}
