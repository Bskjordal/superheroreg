import { Component } from '@angular/core';
import { Heroes } from '../models/heroes.model';
import { HeroesService } from '../service/heroes.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-change-hero',
  templateUrl: './change-hero.component.html',
  styleUrls: ['./change-hero.component.css']
})
export class ChangeHeroComponent {
  showPopup: boolean = false;
  showRecAdded: boolean = false;
  Hero: Heroes = new Heroes(0, "", "", "", "", "", new Date(Date.now()), "");

  constructor(private HeroesService: HeroesService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    
    this.activatedRoute.queryParams.subscribe((params) => {
      let heroName = params['HeroName'];

      this.HeroesService.getDataByName(heroName).subscribe((data: any) => {
        this.Hero = (data[0]);
      })
    });

  }

  onSubmit() {
    this.showPopup = false;
    this.showRecAdded = false;

    if (this.Hero.name == "" || this.Hero.nickname == "" || this.Hero.phone == "" || this.Hero.superpower1 == "" || this.Hero.src == "") {
      this.showPopupWin()
    }
    else {
      
      this.HeroesService.updateHero(this.Hero).subscribe(data => console.dir(data));
      this.closePopup();
      this.RecAdded();

    };

  }

  showPopupWin(): void {
    this.showPopup = true;

  }

  closePopup(): void {
    this.showPopup = false;

  }
  RecAdded(): void {
    this.showRecAdded = true;

  }


}
