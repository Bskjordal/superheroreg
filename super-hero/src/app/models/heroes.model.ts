export class Heroes {
    constructor(
        public id: number,
        public name: string,
        public nickname: string,
        public superpower1: string,
        public superpower2: string,
        public phone: string,
        public dob: Date,
        public src: string
    )
    {}
}
