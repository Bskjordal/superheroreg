import { Component, OnInit } from '@angular/core';
import { Heroes } from '../models/heroes.model';
import { HeroesService } from '../service/heroes.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit{
  HeroArray: Array<Heroes> = [];  

  constructor(private HeroesService: HeroesService, private router: Router) { }

  ngOnInit(): void {

    this.HeroesService.getHero().subscribe((data: any) => {
      this.HeroArray = data;    
    
    })    
    
  }

  DataByName(name: string) {    

      this.HeroesService.getDataByName(name).subscribe((data: any) => {
        this.HeroArray = (data);
      });
  }

  showPopup = false;
  showPopupWin(): void {
    this.showPopup = true;
  }
  
  closePopup(): void {
    this.showPopup = false;
    window.location.reload();
  }
}





