import { Component, OnInit } from '@angular/core';
import { Heroes } from '../models/heroes.model';
import { HeroesService } from '../service/heroes.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-delete-hero',
  templateUrl: './delete-hero.component.html',
  styleUrls: ['./delete-hero.component.css']
})
export class DeleteHeroComponent implements OnInit {
  HeroArray: Array<Heroes> = [];
  searchName!: string;  
  searchResults: Array<Heroes> = [];  
  selectedByName!: string;
  selectedByPower!: string;
  showPopup: boolean = false;
  showRecDeleted: boolean = false;

  constructor(private HeroesService: HeroesService, private router: Router) { }

  ngOnInit(): void {

    this.HeroesService.getHero().subscribe((data: any) => {
      this.HeroArray = data;

    })

  }

  DeleteHero(id: number) {    

    this.HeroesService.deleteHeroById(id).subscribe((data: any) => {
      this.HeroArray = (data);
      this.closePopup();
       this.RecDeleted();
       
    });
}

  search() {        

    if (this.searchName  &&  this.selectedByName =="byname") {
      this.searchResults = this.HeroArray.filter(item => item.name.toLowerCase().includes(this.searchName.toLowerCase()));
      
    }
    if (this.searchName  &&  this.selectedByName =="bypower") {
      this.searchResults = this.HeroArray.filter(item => item.superpower1.toLowerCase().includes(this.searchName.toLowerCase())|| item.superpower2.toLowerCase().includes(this.searchName.toLowerCase()));
           
    }
    if (this.selectedByName =="byall") {
      this.searchResults = this.HeroArray;
           
    }
  }

  showPopupWin(): void {       
    this.showPopup = true;
    
  }
  
  closePopup(): void {   
    this.showPopup = false;
    
  }
  RecDeleted(): void {  
    this.showRecDeleted = true;    
    this.HeroesService.getHero().subscribe((data: any) => {
      this.HeroArray = data;
      this.search();
    })   
   //  window.location.reload();            
    
  }
}
