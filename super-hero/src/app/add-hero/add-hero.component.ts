import { Component } from '@angular/core';
import { Heroes } from '../models/heroes.model';
import { HeroesService } from '../service/heroes.service';
import { Router } from '@angular/router';
import { VirtualTimeScheduler } from 'rxjs';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-add-hero',
  templateUrl: './add-hero.component.html',
  styleUrls: ['./add-hero.component.css']
})
export class AddHeroComponent { 
  newHero:Heroes = new Heroes(0,"","","","","",new Date (Date.now()),"");
  showPopup: boolean = false;
  showRecAdded: boolean = false;
  constructor(private HeroesService: HeroesService){}
  

  onSubmit(){
    this.showPopup = false;
    this.showRecAdded = false;    
    
     if (this.newHero.name == "" || this.newHero.nickname == "" || this.newHero.phone == "" || this.newHero.superpower1 == "" || this.newHero.src == "")
     {
      this.showPopupWin()
     }     
     else{      
       this.HeroesService.addHero(this.newHero).subscribe(data => console.dir(data));       
       this.closePopup();
       this.RecAdded();

     };  
    
  }
    
  showPopupWin(): void {       
    this.showPopup = true;
    
  }
  
  closePopup(): void {   
    this.showPopup = false;
    
  }
  RecAdded(): void {  
    window.location.reload(); 
    this.showRecAdded = true;      
                
    
  }

}

