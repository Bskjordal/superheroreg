import { Injectable } from '@angular/core';
import { Heroes } from '../models/heroes.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, of } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HeroesService {
  
  constructor(private http: HttpClient) { }

  private HeroList: string = 'http://localhost:5114/api/Hero'  

  private httpOptions = {
    headers: new HttpHeaders({
      'Contect-Type': 'application/json'
    })

  };

  getHero(): Observable<Heroes[]> {
    return this.http.get(this.HeroList,
      this.httpOptions)
      .pipe(map(res => <Heroes[]>res));
  }

  addHero(newHero: Heroes): Observable<Heroes[]> {
    return this.http.post(this.HeroList,
      {
        name: newHero.name,
        nickname: newHero.nickname,
        superpower1: newHero.superpower1,
        superpower2: newHero.superpower2,
        phone: newHero.phone,
        dob: newHero.dob,
        src: newHero.src
      },
      this.httpOptions)
      .pipe(map(res => <Heroes[]>res));
  }
  updateHero(newHero: Heroes): Observable<Heroes[]> {
    
    return this.http.put(this.HeroList + '/' + [newHero.id],
      {
        id: newHero.id,
        name: newHero.name,
        nickname: newHero.nickname,
        superpower1: newHero.superpower1,
        superpower2: newHero.superpower2,
        phone: newHero.phone,
        dob: newHero.dob,
        src: newHero.src
      },
      this.httpOptions)
      .pipe(map(res => <Heroes[]>res));
  }
  getDataByName(name: string): Observable<Heroes[]> {    
    return this.http.get(this.HeroList + '/name/' + [name],
      this.httpOptions)
      .pipe(map(res => <Heroes[]>res));    

  }

  deleteHeroById(id: number): Observable<{}> {

     let url = this.HeroList + "/" + id;
    
     return this.http.delete(url, this.httpOptions);
    
     }

}
