import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HomePageComponent } from './home-page/home-page.component';
import { AddHeroComponent } from './add-hero/add-hero.component';
import { ChangeHeroComponent } from './change-hero/change-hero.component';
import { DeleteHeroComponent } from './delete-hero/delete-hero.component';
import { HttpClientModule } from '@angular/common/http';
import { HeroesService } from './service/heroes.service';
import { FaqPageComponent } from './faq-page/faq-page.component';
import { ContactPageComponent } from './contact-page/contact-page.component';

const appRoutes: Routes = [
  { path: "", component: HomePageComponent },
  { path: "add-hero", component: AddHeroComponent },
  { path: "change-hero", component: ChangeHeroComponent }, 
  { path: "delete-hero", component: DeleteHeroComponent },
  { path: "faq-page", component: FaqPageComponent }, 
  { path: "contact-page", component: ContactPageComponent }
   
];

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    AddHeroComponent,
    ChangeHeroComponent,
    DeleteHeroComponent,
    FaqPageComponent,
    ContactPageComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    HttpClientModule
  ],
  providers: [HeroesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
